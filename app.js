// [SECTION]Package and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const courseRoutes = require('./routes/courses');
const userRoutes = require('./routes/users');

// [SECTION]Server Setup
const app = express();
app.use(express.json());
dotenv.config();
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT;

// [SECTION] Application Routes

app.use('/courses', courseRoutes);
app.use('/users', userRoutes);
// [SECTION] Database Connect
mongoose.connect(secret);
let connectStatus = mongoose.connection;
connectStatus.once('open', () => console.log(`Database is connected`));

// [SECTION]Gateway Response
app.get('/', (req, res) => {
     res.send(`Welcome to ENRIQUE's COURSE BOOKING APP`); 
});

app.listen(port, () => console.log(`Server is running on port ${port}`));
