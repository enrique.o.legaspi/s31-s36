//[SECTION] Dependencies and Module
const exp = require("express");
const controller = require('./../controllers/users.js')
const auth = require("../auth");

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routing - [POST]
route.post('/register', (req, res) => {
	let userDetails = req.body
	controller.registerUser(userDetails).then(outcome => {
		res.send(outcome);
		
	})

})

// [SECTION] Login
route.post("/login", controller.loginUser);
// GET USER DETAILS
route.get("/getUserDetails", verify, controller.getUserDetails)

// route.post('/login', (req, res) => {
// 	let userDetails = req.body;
// 	controller.loginUser(userDetails).then(outcome => {
// 		res.send(outcome);
// 	});

// })

// [SECTION] Enroll Registered Users
route.post('/enroll', verify, controller.enroll)


//GET LOGGED USER'S ENROLLMENTS
route.get('/getEnrollments', verify, controller.getEnrollments)


//[SECTION] Routing - [GET]
route.get('/all', (req, res) => {
	controller.getAllUsers().then(result => {
		res.send(result);
	})

})
//[SECTION] Routing - [PUT]
//[SECTION] Routing - [DELETE]
//[SECTION] Routing - Expose Route System
module.exports = route;