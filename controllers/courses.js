//[SECTION] Dependencies and Modules
	const Course = require('./../models/course');

//[SECTION] Functionality [CREATE] 
   module.exports.createCourse = (info) => { 
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;

     let newCourse = new Course({
     	name: cName,
     	description: cDesc,
     	price: cCost
     })

   	 return newCourse.save().then((savedCourse, error) => {

   	 	if (error) {
   	 		return 'Failed to Save New Document';
   	 	} else {
   	 		return savedCourse; 
   	 	}
   	 });
     
   };

//[SECTION] Functionality [RETRIEVE]
module.exports.getAllCourse = () => {
  return Course.find({}).then(result => {
    return result;
  })

}

// Retrieve a single course
module.exports.getCourse = (id) => {
  return Course.findById(id).then(resultOfQuery => {
    return resultOfQuery;
  })

}
// Retrieve all active courses
module.exports.getAllActiveCourse = () => {
  return Course.find({isActive: true}).then(resultOfTheQuery => {
    return resultOfTheQuery;
  })

}

//[SECTION] Functionality [UPDATE]
module.exports.updateCourse = (id, details) => {
  let cName = details.name;
  let cDesc = details.description;
  let cCost = details.price; 

  let updatedCourse = {
    name: cName,
    description: cDesc,
    price: cCost
  }
  return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, error) => {
    if (error) {
      return 'Failed to Update Course';
    } else {
      return 'Course Successfully Updated';
    }
  })

}

// Deactivate a Course
module.exports.deactivateCourse = (id) => {
  let updates = {
    isActive: false
  }
  return Course.findByIdAndUpdate(id, updates).then((archived, error) => {
    if (archived) {
      return `Course ${id} have been archived.`;
    } else {
      return 'Failed to Archive Course';
    }
  })

}

// Reactivate a Course
module.exports.reactivateCourse = (id) => {
  let update = {
    isActive: true
  }
  
  return Course.findByIdAndUpdate(id, update).then((restored, error) => {
    if (restored) {
      return `Course ${id} has been restored.`;
    } else {
      return 'Unable to Restore Course';
    }

  })

}


//[SECTION] Functionality [DELETE]
module.exports.deleteCourse = (id) => {
  return Course.findByIdAndRemove(id).then((removedCourse, err) => {
    if (err) {
      return 'Failed to Remove Course';
    } else {
      return 'Course Successfully Deleted.';
    }
  });

}